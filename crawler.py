#!/usr/bin/env python
# -*- coding: utf-8 -*-

import Tkinter
import tkMessageBox

import re
import os
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import json
import twitter
import nltk.classify
import nltk
from web.twitter_oauth_login import oauth_login
from db import TweetModel

from jinja2 import Environment, FileSystemLoader
import cherrypy

tweetmdl = TweetModel()
    
def search( t, q=None, max_batches=5, count=1000):

    # See https://dev.twitter.com/docs/api/1.1/get/search/tweets
    search_results = t.search.tweets(q=q, count=count)

    statuses = search_results['statuses']

    # Iterate through more batches of results by following the cursor
    for _ in range(max_batches):
        try:
            next_results = search_results['search_metadata']['next_results']
        except KeyError, e: # No more results when next_results doesn't exist
            break
            # Create a dictionary from next_results, which has the following form:
            # ?max_id=313519052523986943&q=%23MentionSomeoneImportantForYou&include_entities=1
            kwargs = dict([ kv.split('=') for kv in next_results[1:].split("&") ])
            search_results = twitter_api.search.tweets(**kwargs)
            statuses += search_results['statuses']

    return statuses

def proses_crawler( keyword=None):
    t = oauth_login()
    Q = keyword
    key = keyword

    file_name = Q+".json" 
    statuses = search(t, q=Q)
    json_data = json.dumps(statuses, indent=1)
    print json_data
    python_obj = json.loads(json_data)

    # proses untuk output file di directory out
    f = open(os.path.join(os.getcwd(), 'out', file_name), 'w')
    f.write(json_data)
    f.close()

    # input to db
    jml_insert = 0
    jml_insert = input_batch_tweet(python_obj, key)

    return jml_insert

def input_batch_tweet( python_obj=None, key=None):
    jml_insert = 0
    for item in python_obj:
        
        # insert tweet into table tweet ori
        id_tweet = item['id']
        screen_name = item['user']['screen_name']
        time_create = item['created_at']
        text = item['text']
        #Convert www.* or https?://* to URL
        text = re.sub('((www\.[\s]+)|(https?://[^\s]+))','URL',text)
        #Replace #word with word
        text = re.sub(r'#([^\s]+)', r'\1', text)
        text = text.replace("'", "")
        text = text.replace('"', "")
        insert = insert_tweet_ori(text, id_tweet, screen_name, time_create, key)
        jml_insert = jml_insert + int(insert)

    return jml_insert

def insert_tweet_ori(text=None, id_tweet=None, screen_name=None, time_create=None, key=None):
    jml_insert = 0
    jml_insert = tweetmdl.insert_tweet_ori(text, id_tweet, screen_name, time_create, key)
    return jml_insert

def proses_crawler_tweet_test( keyword=None):
    t = oauth_login()
    Q = keyword

    file_name = Q+".json" 
    statuses = search(t, q=Q)
    json_data = json.dumps(statuses, indent=1)
    print json_data
    python_obj = json.loads(json_data)

    # proses untuk output file di directory out
    f = open(os.path.join(os.getcwd(), 'out', file_name), 'w')
    f.write(json_data)
    f.close()

    # input to db
    input_batch_tweet_test(python_obj)

def input_batch_tweet_test( python_obj=None):

    for item in python_obj:
        
        # insert tweet into table tweet ori
        id_tweet = item['id']
        screen_name = item['user']['screen_name']
        time_create = item['created_at']
        text = item['text']
        #Convert www.* or https?://* to URL
        text = re.sub('((www\.[\s]+)|(https?://[^\s]+))','URL',text)
        text = text.replace("'", "")
        text = text.replace('"', "")
        insert_tweet_test_crawler(text, id_tweet, time_create)

def insert_tweet_test_crawler(text=None, id_tweet=None, time_create=None):
    tweetmdl.insert_tweet_test_crawler(text, id_tweet, time_create)

if __name__ == '__main__':
    Q = ' '.join(sys.argv[1:])
    jml_insert = 0
    jml_insert = proses_crawler(Q)
    # tkMessageBox.showinfo("proses crawler", Q)
    tkMessageBox.showinfo("jumlah insert data:", jml_insert)
