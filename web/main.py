#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import json
import twitter
import nltk.classify
import nltk
from twitter_oauth_login import oauth_login


from db import TweetModel
from jinja2 import Environment, FileSystemLoader
import cherrypy

env = Environment(loader=FileSystemLoader('templates'), extensions=['jinja2.ext.do'])

class Main:
    
    _cp_config = {'tools.sessions.on': True}
    
    def __init__(self):
        self.tweetmdl = TweetModel()
        self.stopWords = self.getStopWordList('data/feature_list/stopword_list_tala.txt')
        self.data = self.tweetmdl.get_tweet_label_non()
        self.tweet_feature = self.proses_tweet_labeling()
        self.tweetss = self.tweet_feature[0]['tweets']
        self.featureList = self.tweet_feature[0]['featureList']

    
    @cherrypy.expose
    def index(self, tag=None):
        tmpl = env.get_template('index.html')
        return tmpl.render()
    
    @cherrypy.expose
    def ngetweet(self, tag=None):
        tmpl = env.get_template('tweet_form.html')
        return tmpl.render()
    
    @cherrypy.expose
    def open_file_txt(self, tag=None):
        tmpl = env.get_template('open_file_txt.html')
        return tmpl.render()
    
    @cherrypy.expose
    def form_input_tweet_test(self, tag=None):
        tmpl = env.get_template('form_input_tweet_test.html')
        return tmpl.render()
    
    @cherrypy.expose
    def form_input_tweet_labeled(self, tag=None):
        tmpl = env.get_template('form_input_tweet_labeled.html')
        return tmpl.render()

    @cherrypy.expose
    def form_labeling(self, tag=None):
        tmpl = env.get_template('form_labeling.html')
        data = self.tweetmdl.get_tweet_ori_not_labeled()
        data_olah = []

        for item in data:
            
            id_tweet = item['id']
            keyword = item['keyword']
            text = item['tweet']
            text = unicode(text, errors='ignore')
            data_olah.append({'id':id_tweet, 'tweet':text, 'keyword':keyword})

        return tmpl.render(post=data_olah)

    @cherrypy.expose
    def form_crawler(self):
        tmpl = env.get_template('crawler_form.html')
        return tmpl.render()

    @cherrypy.expose
    def form_crawler_test(self):
        tmpl = env.get_template('crawler_form_test.html')
        return tmpl.render()

    @cherrypy.expose
    def view_tweets_ori(self):
        tmpl = env.get_template('view_tweets.html')
        data = self.tweetmdl.get_tweet_ori()
        data_olah = []

        for item in data:
            
            id_tweet = item['id']
            text = item['tweet']
            # text = self.processTweet(text)
            text = unicode(text, errors='ignore')
            data_olah.append({'id':id_tweet, 'tweet':text})


        return tmpl.render(post=data_olah)

    @cherrypy.expose
    def view_tweets_test(self):
        tmpl = env.get_template('view_tweets_test.html')
        data = self.tweetmdl.get_tweet_test()
        return tmpl.render(post=data)

    @cherrypy.expose
    def view_tweets_label(self):
        tmpl = env.get_template('view_tweets_labeled.html')
        data = self.tweetmdl.get_tweet_label()
        # print data
        return tmpl.render(post=data)

    @cherrypy.expose
    def view_tweets_praproses(self):
        tmpl = env.get_template('view_tweets.html')
        data = self.tweetmdl.get_tweet_praproses()
        # print data
        return tmpl.render(post=data)

    @cherrypy.expose
    def insert_tweet_ori(self, text=None, id_tweet=None, screen_name=None, time_create=None, key=None):
        self.tweetmdl.insert_tweet_ori(text, id_tweet, screen_name, time_create, key)

    @cherrypy.expose
    def insert_tweet_test_crawler(self, text=None, id_tweet=None, time_create=None):
        self.tweetmdl.insert_tweet_test_crawler(text, id_tweet, time_create)

    @cherrypy.expose
    def insert_tweet_test(self, tweet=None):
        self.tweetmdl.insert_tweet_test(tweet)
        # redirect url
        raise cherrypy.HTTPRedirect('/view_tweets_test')

    @cherrypy.expose
    def insert_tweet_label(self, tweet_label=None, sentimen=None, tweet_ori_id=None, key=None):
        self.tweetmdl.insert_tweet_label(tweet_label, sentimen, tweet_ori_id, key)

    @cherrypy.expose
    def insert_tweet_label_manual(self, tweet=None, sentiment=None):
        self.tweetmdl.insert_tweet_label_manual(tweet, sentiment)

        # refresh after tweet labeling inserted
        self.data = self.tweetmdl.get_tweet_label_non()
        self.tweet_feature = self.proses_tweet_labeling()
        self.tweetss = self.tweet_feature[0]['tweets']
        self.featureList = self.tweet_feature[0]['featureList']
        raise cherrypy.HTTPRedirect('/form_input_tweet_labeled')

    @cherrypy.expose
    def insert_tweet_praproses(self, text=None):
        self.tweetmdl.insert_tweet_praproses(text)
        
    @cherrypy.expose
    def input_batch_tweet(self, python_obj=None, keyword=None):
        # count_success = 0
        # count_failed = 0
        
        key = keyword

        #Doing input batch from python_obj
        for item in python_obj:
            
            # insert tweet into table tweet ori
            id_tweet = item['id']
            screen_name = item['user']['screen_name']
            time_create = item['created_at']
            text = item['text']
            #Convert www.* or https?://* to URL
            text = re.sub('((www\.[\s]+)|(https?://[^\s]+))','URL',text)
            text = text.replace("'", "")
            text = text.replace('"', "")
            self.insert_tweet_ori(text, id_tweet, screen_name, time_create, key)
            
        # redirect url
        raise cherrypy.HTTPRedirect('/view_tweets_ori')
        
    @cherrypy.expose
    def input_batch_tweet_label(self, **sentimen):
        
        test = sentimen
        temp = []
        dictList = []
        
        for key, value in test.iteritems():
            temp = [key,value]
            dictList.append(temp)

        sentimenList = []
        tweetList = []
        keywordList = []

        for key, value in dictList:
            m = re.match(r"(\w+)\_(\w+)", key)
            
            key_list = m.group(1)
            id_list = int(m.group(2))

            item = [id_list, key_list, value]

            if (key_list == 'sentimen'):
                sentimenList.append(item)

            if (key_list == 'tweets'):
                tweetList.append(item)

            if (key_list == 'keyword'):
                keywordList.append(item)

        sentimenList.sort();
        tweetList.sort();
        keywordList.sort();

        listSentimenTweets = sentimenList + tweetList 
        # print listSentimenTweets

        lengthList = len(listSentimenTweets)/2
        length = len(listSentimenTweets) - lengthList

        mergedSentimenTweets = []

        num = 0
        num2 = 0
        num3 = 0
        for item in listSentimenTweets:
            # print item
            if (num < lengthList):
                mergedSentimenTweets.append(item)
            if (num >= lengthList):
                mergedSentimenTweets[num2].append(item[2])
                num2 = num2 + 1
            num = num + 1

        listSentimenTweets2 = mergedSentimenTweets + keywordList

        mergedSentimenTweets2 = []

        num = 0
        num2 = 0
        num3 = 0
        for item in listSentimenTweets2:
            # print item
            if (num < lengthList):
                mergedSentimenTweets2.append(item)
            if (num >= lengthList):
                mergedSentimenTweets2[num2].append(item[2])
                num2 = num2 + 1
            num = num + 1

        for item in mergedSentimenTweets2:

            print item
            tweet_ori_id = item[0]
            sentimen = item[2]
            tweet_label = item[3]
            keyword = item[4]

            self.insert_tweet_label(tweet_label, sentimen, tweet_ori_id, keyword)


        # for item in test.iteritems():
        #     # ind =  "{}{}".format("sentimen_", num)
        #     # ind_2 =  "{}{}".format("tweets_", num)

        #     print item


            # num = num + 1


        # ind_2 =  "{}{}".format("tweets_", 1)
        # ind =  "{}{}".format("tweets_", num)
        # tweet = test[ind_2]
        # sentimen = test[ind]
        # print tweet
        # print test

        # refresh after tweet labeling inserted
        self.data = self.tweetmdl.get_tweet_label_non()
        self.tweet_feature = self.proses_tweet_labeling()
        self.tweetss = self.tweet_feature[0]['tweets']
        self.featureList = self.tweet_feature[0]['featureList']

        raise cherrypy.HTTPRedirect('/view_tweets_label')
        
    @cherrypy.expose
    def input_batch_tweetFromFile(self, path_file=None):

        data = self.getTweetFromFileTxt(path_file)

        # input batch from data
        for item in data:
            # proses pemotongan sesuai format yang ada di dalam text
            part = item.partition('||')
            time_create = part[0]
            tweet = part[2]
            screen_name = ""
            id_tweet = ""
            print tweet
            tweetProcessed = self.processTweet(tweet)
            # insert tweet into table tweet ori labeled
            self.insert_tweet_ori(tweetProcessed, id_tweet, screen_name, time_create)

        # redirect url
        raise cherrypy.HTTPRedirect('/view_tweets_ori')

    @cherrypy.expose
    def input_batch_tweet_test(self, python_obj=None):

        for item in python_obj:
            
            # insert tweet into table tweet ori
            id_tweet = item['id']
            screen_name = item['user']['screen_name']
            time_create = item['created_at']
            text = item['text']
            #Convert www.* or https?://* to URL
            text = re.sub('((www\.[\s]+)|(https?://[^\s]+))','URL',text)
            text = text.replace("'", "")
            text = text.replace('"', "")
            self.insert_tweet_test_crawler(text, id_tweet, time_create)

        # redirect url
        raise cherrypy.HTTPRedirect('/view_tweets_test')

# ===============================================================================
# praproses and extract function
# ===============================================================================

    #start process_tweet
    def processTweet(self, tweet):
        # process the tweets

        #Convert to lower case
        tweet = tweet.lower()
        #Convert www.* or https?://* to URL
        tweet = re.sub('((www\.[\s]+)|(https?://[^\s]+))','URL',tweet)
        #Convert @username to AT_USER
        tweet = re.sub('@[^\s]+','AT_USER',tweet)
        #Remove additional white spaces
        tweet = re.sub('[\s]+', ' ', tweet)
        #Replace #word with word
        tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
        #trim
        tweet = tweet.strip('\'"')
        return tweet
    #end

    #start getStopWordList
    def getStopWordList(self, stopWordListFileName):
        #read the stopwords file and build a list
        stopWords = []
        stopWords.append('AT_USER')
        stopWords.append('URL')
     
        fp = open(stopWordListFileName, 'r')
        line = fp.readline()
        while line:
            word = line.strip()
            stopWords.append(word)
            line = fp.readline()
        fp.close()
        return stopWords
    #end

    #start getTweetFromFileTxt
    def getTweetFromFileTxt(self, tweetsFileName):
        #read the stopwords file and build a list
        tweetFromFile = []
        tweetFromFile.append('AT_USER')
        tweetFromFile.append('URL')
     
        fp = open(tweetsFileName, 'r')
        line = fp.readline()
        while line:
            word = line.strip()
            tweetFromFile.append(word)
            line = fp.readline()
        fp.close()
        return tweetFromFile
    #end

    #start replaceTwoOrMore
    def replaceTwoOrMore(self, s):
        #look for 2 or more repetitions of character and replace with the character itself
        pattern = re.compile(r"(.)\1{1,}", re.DOTALL)
        return pattern.sub(r"\1\1", s)
    #end
     
    #start getfeatureVector
    def getFeatureVector(self, tweet, stopWords):
        featureVector = []
        #split tweet into words
        words = tweet.split()
        for w in words:
            #replace two or more with two occurrences
            w = self.replaceTwoOrMore(w)
            #strip punctuation
            w = w.strip('\'"?,.')
            #check if the word stats with an alphabet
            val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*$", w)
            #ignore if it is a stop word
            if(w in stopWords or val is None):
                continue
            else:
                featureVector.append(w.lower())
        return featureVector
    #end

# ===============================================================================

    #start extract_features
    @cherrypy.expose
    def extract_features(self, tweet):
        tweet_words = set(tweet)
        features = {}

        # dataFeatures = self.proses_ekstrak_fitur()

        for word in self.featureList:
            features['contains(%s)' % word] = (word in tweet_words)

        return features
    #end

    @cherrypy.expose
    def proses_ekstrak_fitur(self):

        # proses ambil list stopword indonesia
        featureList = []
        tweets = []
        # membaca dari file open
        st = open('data/feature_list/stopword_list_tala.txt', 'r')
        stopWords = self.getStopWordList('data/feature_list/stopword_list_tala.txt')

        # mengambil tweet yang sudah labeled untuk data training
        # data = self.tweetmdl.get_tweet_label()

        # proses insert to table at database tweets_db
        for item in self.data:

            # processTweet
            tweetProcessed = self.processTweet(item['tweet'])
            featureVector = self.getFeatureVector(tweetProcessed, stopWords)

            featureList.extend(featureVector)
            featureList = list(set(featureList))

        return featureList

    @cherrypy.expose
    def proses_tweet_labeling(self):

        result_tweet_feature = []
        featureList = []
        tweets = []
        num = 1
        ntweet = 0

        print "======== PROSES_TWEET_LABELED ========"

        for item in self.data:

            # processTweet
            tweetProcessed = self.processTweet(item['tweet'])
            sentimen = self.processTweet(item['sentimen'])
            featureVector = self.getFeatureVector(tweetProcessed, self.stopWords)

            featureList.extend(featureVector)
            featureList = list(set(featureList))

            tweets.append((featureVector, sentimen));

            featureVectorPerTweet = tweets[ntweet][0]
            sentimentPerTweet = tweets[ntweet][1]
            num = num+1
            ntweet = ntweet+1

            print sentimentPerTweet,"-", featureVectorPerTweet

        result_tweet_feature.append({'tweets':tweets, 'featureList':featureList})

        return result_tweet_feature

    @cherrypy.expose
    def proses_clasiffier_pertweet(self, tweet=None):

        st = open('data/feature_list/stopword_list_tala.txt', 'r')
        stopWords = self.getStopWordList('data/feature_list/stopword_list_tala.txt')

        sentimen_result_maxent = "none"
        sentimen_result_nbc = "none"

        # # Extract feature vector for all tweets in one shote
        training_set = nltk.classify.util.apply_features( self.extract_features, self.tweetss )
        processedTestTweet = self.processTweet(tweet)

        #NBC Classifier
        NBClassifier = nltk.NaiveBayesClassifier.train(training_set)
        sentimen_result_nbc = NBClassifier.classify(self.extract_features(self.getFeatureVector(processedTestTweet, \
                                self.stopWords)))
        NBClassifier.show_most_informative_features(500)
        print sentimen_result_nbc

        # #Max Entropy Classifier
        MaxEntClassifier = nltk.classify.maxent.MaxentClassifier.train(training_set, 'GIS', trace=3, \
                               encoding=None, labels=None, sparse=True, gaussian_prior_sigma=0, max_iter = 10)
        sentimen_result_maxent = MaxEntClassifier.classify(self.extract_features(self.getFeatureVector(processedTestTweet,\
                                    self.stopWords)))
        print sentimen_result_maxent

        tmpl = env.get_template('view_clasification_result_pertweet.html')

        results = []
        results.append({'tweet':tweet, 'sentimen_by_nbc':sentimen_result_nbc, 'sentimen_by_maxent':sentimen_result_maxent})

        return tmpl.render(post=results)

    @cherrypy.expose
    def proses_clasiffier(self, data_test=None):
        result_classification = []
        st = open('data/feature_list/stopword_list_tala.txt', 'r')
        stopWords = self.getStopWordList('data/feature_list/stopword_list_tala.txt')

        # # Extract feature vector for all tweets in one shote
        training_set = nltk.classify.util.apply_features( self.extract_features, self.tweetss )

        #Max Entropy Classifier
        MaxEntClassifier = nltk.classify.maxent.MaxentClassifier.train(training_set, 'GIS', trace=3, \
                               encoding=None, labels=None, sparse=True, gaussian_prior_sigma=0, max_iter = 10)

        for item in data_test:            
            processedTestTweet = self.processTweet(item['tweet'])
            sentimen_result = MaxEntClassifier.classify(self.extract_features(self.getFeatureVector(processedTestTweet, \
                                self.stopWords)))
            print sentimen_result
            result_classification.append({'tweet':item['tweet'], 'sentimen':sentimen_result})

        return result_classification

    @cherrypy.expose
    def proses_test_clasiffication(self):
        
        data_test = self.tweetmdl.get_tweet_test()

        result = self.proses_clasiffier(data_test)

        print result

        tmpl = env.get_template('view_clasification_result.html')

        return tmpl.render(post=result)

    @cherrypy.expose
    def search(self, t, q=None, max_batches=5, count=100):

        # See https://dev.twitter.com/docs/api/1.1/get/search/tweets
        search_results = t.search.tweets(q=q, count=count)

        statuses = search_results['statuses']

        # Iterate through more batches of results by following the cursor
        for _ in range(max_batches):
            try:
                next_results = search_results['search_metadata']['next_results']
            except KeyError, e: # No more results when next_results doesn't exist
                break
                # Create a dictionary from next_results, which has the following form:
                # ?max_id=313519052523986943&q=%23MentionSomeoneImportantForYou&include_entities=1
                kwargs = dict([ kv.split('=') for kv in next_results[1:].split("&") ])
                search_results = twitter_api.search.tweets(**kwargs)
                statuses += search_results['statuses']

        return statuses

    @cherrypy.expose
    def proses_crawler(self, keyword=None):
        t = oauth_login()
        Q = keyword

        statuses = self.search(t, q=Q)
        json_data = json.dumps(statuses, indent=1)
        print json_data
        python_obj = json.loads(json_data)

        # proses untuk output file di directory out
        file_name = Q+".json" 
        f = open(os.path.join(os.getcwd(), 'out', file_name), 'w')
        f.write(json_data)
        f.close()

        # input to db
        self.input_batch_tweet(python_obj, Q)

    @cherrypy.expose
    def proses_crawler_tweet_test(self, keyword=None):
        t = oauth_login()
        Q = keyword

        file_name = Q+".json" 
        statuses = self.search(t, q=Q)
        json_data = json.dumps(statuses, indent=1)
        print json_data
        python_obj = json.loads(json_data)

        # proses untuk output file di directory out
        f = open(os.path.join(os.getcwd(), 'out', file_name), 'w')
        f.write(json_data)
        f.close()

        # input to db
        self.input_batch_tweet_test(python_obj)

        
