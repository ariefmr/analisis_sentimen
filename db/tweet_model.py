from database import *
import datetime

class TweetModel:
    
    def __init__(self):
        self.db = db
        self.cursor = cursor
    
    def get_tweet_ori_not_labeled(self):
        sql = "select id_tweet_ori, tweet_ori, keyword from tweet_ori where is_label='0' order by id_tweet_ori desc \
                limit 0,30"
        try :
            self.cursor.execute(sql)
            temp_results = self.cursor.fetchall()
            results = []
            i = 1
            for row in temp_results:
                num = i
                results.append({'num':num, 'id':row[0], 'tweet':row[1], 'keyword':row[2]})
                i = i + 1

            return results

        except:
            print "Error : tidak bisa mengambil data"

    def get_tweet_ori(self):
        sql = "select id_tweet_ori, tweet_ori from tweet_ori order by id_tweet_ori desc"
        try :
            self.cursor.execute(sql)
            temp_results = self.cursor.fetchall()
            results = []
            i = 1
            for row in temp_results:
                num = i
                results.append({'num':num, 'id':row[0], 'tweet':row[1]})
                i = i + 1

            return results

        except:
            print "Error : tidak bisa mengambil data"
    
    def get_tweet_test(self):
        sql = "select id_tweet_test, tweet_test from tweet_test order by id_tweet_test desc"
        try :
            self.cursor.execute(sql)
            temp_results = self.cursor.fetchall()
            results = []
            i = 1
            for row in temp_results:
                num = i
                results.append({'num':num, 'id':row[0], 'tweet':row[1]})
                i = i + 1

            return results

        except:
            print "Error : tidak bisa mengambil data"
    
    def get_tweet_label_non(self):
        # sql = "select id_tweet_label, tweet_label, sentimen from tweet_label "
        sql = "select id_tweet_label, tweet_label, sentimen from tweet_label where sentimen = 'negatif' or sentimen ='positif' "
        try :
            self.cursor.execute(sql)
            temp_results = self.cursor.fetchall()
            # print type(temp_results)
            results = []
            i = 1
            for row in temp_results:
                num = i
                results.append({'num':num, 'id':row[0], 'tweet':row[1], 'sentimen':row[2]})
                i = i + 1

            return results

        except:
            print "Error : tidak bisa mengambil data"
    
    def get_tweet_label(self):
        sql = "select id_tweet_label, tweet_label, sentimen from tweet_label where sentimen = 'negatif' or sentimen = 'positif'  order \
                by id_tweet_label desc"
        try :
            self.cursor.execute(sql)
            temp_results = self.cursor.fetchall()
            # print type(temp_results)
            results = []
            i = 1
            for row in temp_results:
                # print "arief", i
                # print row[0]
                # print row[1]
                num = i
                results.append({'num':num, 'id':row[0], 'tweet':row[1], 'sentimen':row[2]})
                i = i + 1

            return results

        except:
            print "Error : tidak bisa mengambil data"
    
    def get_tweet_praproses(self):
        sql = "select id_tweet_praproses, tweet_praproses from tweet_praproses"
        try :
            self.cursor.execute(sql)
            temp_results = self.cursor.fetchall()
            # print type(temp_results)
            results = []
            i = 1
            for row in temp_results:
                # print "arief", i
                # print row[0]
                # print row[1]
                num = i
                results.append({'num':num, 'id':row[0], 'tweet':row[1]})
                i = i + 1

            return results

        except:
            print "Error : tidak bisa mengambil data"

    def insert_tweet_ori(self, text, id_tweet, screen_name, time_create, key):
        temp_tanggal = datetime.datetime.now()
        sql = "insert into tweet_ori (tweet_ori, id_tweet, screen_name, time_create, keyword) values \
                ('%s', '%s', '%s', '%s', '%s')" % (text, id_tweet, screen_name, time_create, key) 
        try:
            self.cursor.execute(sql)
            db.commit()
            print "INSERT SUCCESS : ",text
            return 1
        except:
            db.rollback()
            print "INSERT FAILED : ",text
            return 0

    def insert_tweet_praproses(self, text):
        temp_tanggal = datetime.datetime.now()
        sql = "insert into tweet_praproses (tweet_praproses) values ('%s')" % (text) 
        try:
            self.cursor.execute(sql)
            db.commit()
            print "INSERT SUCCESS"
        except:
            db.rollback()
            print "INSERT FAILED : ",text

    def insert_tweet_test(self, text):
        temp_tanggal = datetime.datetime.now()
        sql = "insert into tweet_test (tweet_test) values ('%s')" % (text) 
        try:
            self.cursor.execute(sql)
            db.commit()
            print "INSERT SUCCESS"
        except:
            db.rollback()
            print "INSERT FAILED : ",text

    def insert_tweet_test_crawler(self, text, id_tweet, time_create):
        temp_tanggal = datetime.datetime.now()
        sql = "insert into tweet_test (tweet_test, id_tweet, time_create) values ('%s', '%s', '%s')" % \
                (text, id_tweet, time_create) 
        try:
            self.cursor.execute(sql)
            db.commit()
            print "INSERT SUCCESS"
        except:
            db.rollback()
            print "INSERT FAILED : ",text

    def insert_tweet_label(self, tweet_label, sentimen, tweet_ori_id, key):
        temp_tanggal = datetime.datetime.now()
        sql = "insert into tweet_label (tweet_label, sentimen, tweet_ori_id, keyword) values ('%s', '%s', '%s', '%s')" % \
                (tweet_label, sentimen, tweet_ori_id, key) 
        try:
            self.cursor.execute(sql)
            db.commit()
            print "INSERT SUCCESS"
            sql_2 = "update tweet_ori set is_label = '%s' where id_tweet_ori = tweet_ori_id"
            sql_2 = "update tweet_ori set is_label='%d' where id_tweet_ori=%d" % (1, int(tweet_ori_id))
            self.cursor.execute(sql_2)
            db.commit()
            print "UPDATE SUCCESS"
        except:
            db.rollback()
            print "INSERT FAILED : ",tweet_label

    def insert_tweet_label_manual(self, text, sentimen):
        temp_tanggal = datetime.datetime.now()
        sql_1 = "insert into tweet_ori (tweet_ori, is_label) values ('%s', '%s')" % (text, "1") 
        try:
            self.cursor.execute(sql_1)
            id_tweet_ori = self.cursor.lastrowid
            db.commit()
            print "INSERT1 SUCCESS"
            sql_2 = "insert into tweet_label (tweet_label, sentimen, tweet_ori_id) values ('%s', '%s', '%d')" % \
                        (text, sentimen, id_tweet_ori) 
            self.cursor.execute(sql_2)
            db.commit()
            print "INSERT2 SUCCESS"
        except:
            db.rollback()
            print "INSERT FAILED : ",text
            
    def update_post(self, judul, tag, isi, id_note):
        temp_tanggal = datetime.datetime.now()
        sql = "update note set judul='%s', tag='%s', isi='%s', tanggal_diperbaharui='%s' where id_note=%d" % \
                (judul, tag, isi, temp_tanggal.strftime('%Y-%m-%d %H-%M-%S'), int(id_note)) 
        try:
            self.cursor.execute(sql)
            db.commit()
            print "Info : data berhasil diubah.."
        except:
            db.rollback()
            print "Error : pengubahan data gagal.."
            